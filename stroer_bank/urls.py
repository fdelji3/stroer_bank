from django.urls import path, include
from django.contrib import admin
from django.urls import path
from users.views import base

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', base,name="base_site"),
    path('', include('google_oauth_app.urls')),
    ]