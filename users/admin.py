from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from .models import UserProfile

class UserProfileInline(admin.StackedInline):
    model = UserProfile
    list_display = ('first_name', 'last_name', 'iban')
    can_delete = False
    verbose_name = "User Profile"
    verbose_name_plural = 'User Profiles'
    @admin.display(empty_value='')
    def view_birth_date(self, obj):
        return obj.first_name
# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (UserProfileInline,)

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)